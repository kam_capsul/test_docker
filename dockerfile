FROM node:alpine

WORKDIR /
COPY ./backend ./app

RUN cd ./app && npm install

EXPOSE 3000

CMD cd ./app && npm start